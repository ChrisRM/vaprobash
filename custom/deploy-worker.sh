#!/usr/bin/env bash

echo ">>> Deploying Videl Workers"

[[ -z "$1" ]] && { printf "!!! No worker name specified\n    Deployment aborted.\n"; exit 0; }

git --version > /dev/null 2>&1
GIT_IS_INSTALLED=$?

composer -v > /dev/null 2>&1
COMPOSER_IS_INSTALLED=$?

if [[ $GIT_IS_INSTALLED -gt 0 ]]; then
    echo ">>> ERROR: Worker client requires git"
    exit 1
fi

if [[ $COMPOSER_IS_INSTALLED -gt 0 ]]; then
    echo ">>> ERROR: Worker client requires composer"
    exit 1
fi

# Our worker name. Remove any blank spaces
worker_name="$1"
user_name="$2"

# Bitbucket
bitbucket_endpoint="https://bitbucket.org/api/1.0/repositories/videlapp/worker/deploy-keys"
bitbucket_authkey="Q2hyaXNSTTpub2FoMTIzMjE="

# Generate deployment key.
echo -e 'y\n'|ssh-keygen -q -t rsa -N "" -f /home/${user_name}/.ssh/id_${worker_name//[[:space:]]}

# Make sure ssh-agent is started
killall ssh-agent; eval `ssh-agent -s`

# Add new key to our agent
ssh-add /home/${user_name}/.ssh/id_${worker_name//[[:space:]]}

# Set key variable 
ssh_key=`cat /home/${user_name}/.ssh/id_${worker_name//[[:space:]]}.pub`

# Add key to bitbucket
curl -H "Authorization: Basic ${bitbucket_authkey}" -X POST -D - --data-urlencode "key=${ssh_key}" -d "label=${worker_name}" $bitbucket_endpoint

# Initialize git locally and pull down latest worker-library
mkdir /home/${user_name}/public 
cd /home/${user_name}/public
git init
git remote add origin git@bitbucket.org:videlapp/worker.git
git pull origin master

# Update composer
composer update

# Start worker
nohup php work --worker="VideoConverter" &